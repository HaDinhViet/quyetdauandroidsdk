package transport.endpoint;

import transport.EndPoint;

public class QuyetdauEndPoint implements EndPoint {
	private static final String PATTERN = "http://beta.quyetdau.com/api/v%s%s";
	private String version;
	private String path;

	public QuyetdauEndPoint(String version, String path) {
		this.version = version;
		this.path = path;
		if(!path.startsWith("/")) {
			throw new RuntimeException("a path must be leading by /");
		}
	}

	@Override
	public String build() {
		return String.format(PATTERN, this.version, this.path);
	}
}
