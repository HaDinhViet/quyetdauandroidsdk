package transport;

import java.io.Serializable;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

/**
 * 
 * @author hadv
 *
 */
public class Response implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2061777699204821099L;
	public String status;
	public Object data;
	public String code;
	public String message;
	public int httpCode;

	/**
	 * Get an {@link Response} instance from a JSON string
	 * @param jsonString
	 * @return
	 */
	public static Response fromJson(String jsonString) {
		return new Gson().fromJson(jsonString, new TypeToken<Response>(Response.class) {
			private static final long serialVersionUID = -178528780579247158L;
		}.getType());
	}

	@Override
	public String toString() {
		return "Response [status=" + status + ", data=" + data + ", code=" + code + ", message=" + message
				+ ", httpCode=" + httpCode + "]";
	}
}
