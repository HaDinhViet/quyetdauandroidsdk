package transport;

import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONObject;

import transport.HttpHelper.RequestType;

/**
 * 
 * @author hadv
 *
 */
public interface Transporter {
	public String getToken();
	public void setToken(String token);
	public JSONObject send(RequestType requestType, EndPoint endPoint, List<NameValuePair> params) throws Exception;
}
