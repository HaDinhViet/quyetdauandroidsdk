package transport;

public interface EndPoint {
	public String build();
}
