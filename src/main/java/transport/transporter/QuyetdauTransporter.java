package transport.transporter;

import java.net.URI;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.json.JSONObject;

import transport.EndPoint;
import transport.HttpHelper;
import transport.Transporter;
import transport.HttpHelper.RequestType;

public class QuyetdauTransporter implements Transporter {

	private static final String HEADER_TOKEN = "WL-Auth";
	
	private String token;
	
	public QuyetdauTransporter() {}
	

	@Override
	public String getToken() {
		return token;
	}

	@Override
	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public JSONObject send(RequestType requestType, EndPoint endPoint, List<NameValuePair> params) throws Exception {
		HttpHelper helper = new HttpHelper();
		Set<Header> headers = new HashSet<>();
		if(token != null) {
			headers.add(new BasicHeader(HEADER_TOKEN, token));
		}
		HttpResponse response = helper.send(requestType, new URI(endPoint.build()), headers, params);
		JSONObject result = new JSONObject(helper.getString(response));
		result.put("httpCode", response.getStatusLine().getStatusCode());
//		result.httpCode = response.getStatusLine().getStatusCode();
		return result;
	}

}
