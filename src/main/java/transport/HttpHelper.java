package transport;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;

/**
 * HTTP Request helper
 * @author hadv
 *
 */
public class HttpHelper {
	public static final String DEFAULT_CONTENT_ENCODING = "UTF-8";
	public enum RequestType {
		POST,
		GET,
		PATCH,
		PUT,
		DELETE
	}
	
	/**
	 * Send HTTP request
	 * @param requestType
	 * @param uri
	 * @param headers
	 * @param params
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public HttpResponse send(RequestType requestType, URI uri, Set<Header> headers, List<NameValuePair> params) throws UnsupportedEncodingException, IOException, URISyntaxException {
		return getResponse(buildRequest(uri, requestType, headers, params));
	}
	
	/**
	 * Build a request
	 * @param uri
	 * @param requestType
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws URISyntaxException
	 */
	public HttpUriRequest buildRequest(URI uri, RequestType requestType) throws UnsupportedEncodingException, URISyntaxException {
		return buildRequest(uri, requestType, null, null);
	}
	
	/**
	 * Build a request
	 * @param uri
	 * @param requestType
	 * @param headers
	 * @param params
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws URISyntaxException
	 */
	public HttpUriRequest buildRequest(URI uri, RequestType requestType, Set<Header> headers, List<NameValuePair> params) throws UnsupportedEncodingException, URISyntaxException {
		HttpRequestBase request;
		switch(requestType) {
			case POST:
				request = new HttpPost(uri); break;
			case DELETE:
				request = new HttpDelete(uri); break;
			case PUT:
				request = new HttpPut(uri); break;
			default:
				request = new HttpGet(uri); break;
		}
		if(headers != null) {
			headers.forEach(header -> {
				request.addHeader(header);
			});
		}
		
		if(params != null) {
			if(request instanceof HttpEntityEnclosingRequest) {
				((HttpEntityEnclosingRequest) request).setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
			} else {
				URIBuilder builder = new URIBuilder(uri);
				builder.addParameters(params);
				request.setURI(builder.build());
			}
		}
		return request;
		
	}
	
	private HttpResponse getResponse(HttpUriRequest request) throws IOException {
		HttpClient httpclient = HttpClients.createDefault();
		return httpclient.execute(request);
	}
	
	/**
	 * Convert {@link HttpResonse} to string
	 * @param response
	 * @return
	 * @throws UnsupportedOperationException
	 * @throws IOException
	 */
	public String getString(HttpResponse response) throws UnsupportedOperationException, IOException {
		HttpEntity entity = response.getEntity();
		if (entity != null) {
		    InputStream instream = entity.getContent();
		    try {
		    	String encoding = entity.getContentEncoding() != null ? entity.getContentEncoding().getValue() : null;
		    	encoding = encoding == null ? DEFAULT_CONTENT_ENCODING : encoding;
		    	return IOUtils.toString(instream, encoding);
		    } finally {
		        instream.close();
		    }
		}
		return null;
	}
}
