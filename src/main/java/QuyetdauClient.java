import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import transport.HttpHelper.RequestType;
import transport.Transporter;
import transport.endpoint.QuyetdauEndPoint;
import transport.transporter.QuyetdauTransporter;

/**
 * 
 * @author hadv
 *
 */
public class QuyetdauClient {
	public static final class EndPointPath {
		public static final String AUTH_GET_TOKEN = "/auth/get-token";
		public static final String USER_PROFILE = "/user/profile";
	}
	
	private String version = "1";
	private Transporter transporter;
	
	public QuyetdauClient() {
		transporter = new QuyetdauTransporter();
	}

	public QuyetdauClient(String version) {
		this();
		this.version = version;
	}
	
	public void setToken(String token) {
		transporter.setToken(token);
	}

	/**
	 * Get the token for authorization
	 * @param username
	 * @param password
	 * @param fromBackEnd
	 * @return
	 * @throws Exception
	 */
	public String getToken(String username, String password, boolean fromBackEnd) throws Exception {
		JSONObject response = transporter.send(
			RequestType.POST, 
			new QuyetdauEndPoint(version, EndPointPath.AUTH_GET_TOKEN), 
			new ArrayList<NameValuePair>(){
				private static final long serialVersionUID = 4486604239167882738L;
				{
					add(new BasicNameValuePair("username", username));
					add(new BasicNameValuePair("password", password));
					add(new BasicNameValuePair("bs", String.valueOf(fromBackEnd)));
				}
			}
		);
		if(response.getInt("httpCode") != 200) {
			throw new Exception(response.getString("message"));
		}
		return response.getJSONObject("data").get("token").toString();
	}

	public String getUserWishList() throws Exception {
		JSONObject response = transporter.send(
				RequestType.GET, 
				new QuyetdauEndPoint(version, EndPointPath.USER_PROFILE), 
				null
			);
			if(response.getInt("httpCode") != 200) {
				throw new Exception(response.getString("message"));
			}
			return response.toString();
	}
	
}
